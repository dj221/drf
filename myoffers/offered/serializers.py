from rest_framework import serializers
from .models import *


class OffersSerializer(serializers.ModelSerializer):

    class Meta:
        model = Offers
        fields = '__all__'