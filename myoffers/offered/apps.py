from django.apps import AppConfig


class OfferedConfig(AppConfig):
    name = 'offered'
