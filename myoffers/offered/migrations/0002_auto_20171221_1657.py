# Generated by Django 2.0 on 2017-12-21 11:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('offered', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offers',
            name='offer_title',
            field=models.CharField(max_length=150),
        ),
    ]
