from django.db import models
import datetime


class Offers(models.Model):
    offer_title = models.CharField(max_length=150,null=False,blank=False)
    coupon_no = models.IntegerField(null=False,blank=False)
    terms_conditions = models.TextField()
    min_points = models.IntegerField(null=False,blank=False)
    max_points = models.IntegerField(null=False, blank=False)     
    up_to_points = models.IntegerField(null=False,blank=False)
    start_date = models.DateTimeField('valid-from')
    expiry_date = models.DateTimeField('valid-till')
    images = models.ImageField(blank=True,null=True,upload_to="media")

    def __str__(self):
        return self.offer_title

    def offer_time(self): #return self.start_date>=timezone.now()-datetime.timedelta(days=1)
        from datetime import timedelta
        self.start_date=timedelta(days=1)

        if not self.id:
            self.expiry_date = datetime.now() + self.start_date
            super(Offers,self).save()

