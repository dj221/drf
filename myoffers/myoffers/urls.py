from django.contrib import admin
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from offered import views
from django.views.static import serve
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('offers/',views.OffersList.as_view()),
    path('offers/<int:pk>/',views.OffersDetail.as_view())
]

if settings.DEBUG:
    urlpatterns += [
        path('media/(?P<path>.*'),serve,{
            'document_root':settings.MEDIA_ROOT,
        },
    ]

urlpatterns = format_suffix_patterns(urlpatterns)
